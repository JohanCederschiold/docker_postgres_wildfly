
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/all")
public class DBServlet extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest reqeust, HttpServletResponse response) {
		
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		Properties properties = new Properties();
		properties.setProperty("user", "postgres");
		
		Connection connection;
		try {
			connection = DriverManager.getConnection("jdbc:postgresql://mypostgres/", properties);
			Statement statement = connection.createStatement();
			ResultSet results = statement.executeQuery("select * from mytest");
			
			while(results.next()) {
				System.out.printf("Name: %s\n", results.getString(1));
				response.setContentType("text/plain");
				try {
					response.getWriter().println(results.getString(1));
				} catch (IOException e) {
					e.printStackTrace();
				} 
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		
		
		
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) {

		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		Properties properties = new Properties();
		properties.setProperty("user", "postgres");
		
		Connection connection;
		try {
			connection = DriverManager.getConnection("jdbc:postgresql://mypostgres/", properties);
		Statement statement = connection.createStatement();
		statement.execute("create table if not exists mytest(name text)");
		
		PreparedStatement preparedStatement = connection.prepareStatement("insert into mytest values (?)");
		response.setContentType("text/plain");
		String turtleName = request.getParameter("name");
		preparedStatement.setString(1, turtleName);
		preparedStatement.execute();
		
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
	}
	

}

