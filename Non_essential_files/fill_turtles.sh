#!/bin/bash
curl --data 'name=Murdering Myrtle' --request POST -k https://localhost/turtles
curl --data 'name=Groveling Gabriel' --request POST -k https://localhost/turtles
curl --data 'name=Slaying Sally' --request POST -k https://localhost/turtles
curl --data 'name=Cynical Cynthia' --request POST -k https://localhost/turtles
curl --data 'name=Exploding Enrique' --request POST -k https://localhost/turtles
curl --data 'name=Satanic Stan' --request POST -k https://localhost/turtles
curl --data 'name=Kamikaze Kim' --request POST -k https://localhost/turtles
curl --data 'name=Smelly Samson' --request POST -k https://localhost/turtles