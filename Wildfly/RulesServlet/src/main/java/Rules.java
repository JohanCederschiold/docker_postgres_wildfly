import java.io.IOException;
import java.util.Random;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/rules")
public class Rules extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String [] rules = {
			"The turtle that crosses the line first is the winner",
			"If two turtles come in on the same day, they share the winnings",
			"No biting",
			"Seriously...no biting!!!",
			"No..it's not ok to nibble either",
			"We know that you technically don't have teeth. It still counts as biting...",
			"Birthdays taking place during the course of the race shall be celebrated by all",
			"Love is all around us",
			"Yes, you too",
			"If you bark you'd better be a dog!!!",
			"No performance enhancing additives.",
			"Let's keep it clean. Do your \"business\" in the bushes"
	};
	
	
	
	@Override
	protected void doGet (HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		System.out.println("Rules called");
		
		Random random = new Random();
		response.setContentType("text/plain");
		response.getWriter().println("Randomly generated rule:");
		response.getWriter().println(rules[random.nextInt(rules.length)]);


	}

}
